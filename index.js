const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

app.use(express.json());

mongoose.connect(
  "mongodb+srv://nadchif:8sFt16XPJIjJ4ViG@cluster0.io6rf.mongodb.net/todoList123?retryWrites=true&w=majority",
  { useNewUrlParser: true, useUnifiedTopology: true }
);

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error."));
db.once("open", () =>
  console.log(`${new Date().toLocaleTimeString()}| Connected to MongoDB`)
);
const taskRoutes = require("./routes/taskRoutes");
const userRoutes = require("./routes/userRoutes");

// Activity
app.use("/tasks", taskRoutes);

app.use("/users", userRoutes);

app.get("/hello", (req, res) => {
  res.send("Hello from our new Express Api!");
});

app.listen(port, () =>
  console.log(
    `${new Date().toLocaleTimeString()}| Server is running on ${port}`
  )
);
