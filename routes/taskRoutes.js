const express = require("express");
const {
  createTaskController,
  getAllTasksController,
  cancelTaskController,
  completeTaskController,
} = require("../controllers/taskControllers");

const router = express.Router();

router.post("/", createTaskController);

router.get("/", getAllTasksController);

router.put("/complete/:id", completeTaskController);

router.put("/cancel/:id", cancelTaskController);

module.exports = router;
