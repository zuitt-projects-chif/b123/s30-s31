const Task = require("../models/Task");

module.exports.createTaskController = (req, res) => {
  console.log("body", req.body);

  Task.findOne({ name: req.body.name }, (err, result) => {
    console.error("err", err);
    console.log("result", result);
    if (result !== null && result.name === req.body.name) {
      res.status(409).send("Duplicate Task Found");
    } else {
      const newTask = new Task({
        name: req.body.name,
      });
      newTask.save((saveErr, savedTask) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          console.log("Saved", savedTask);
          return res.send("New Task Created");
        }
      });
    }
  });
};

module.exports.getAllTasksController = (req, res) => {
  Task.find({}, (err, result) => {
    console.error("err", err);
    console.log("result", result);
    if (!err) {
      return res.send(result);
    } else {
      return console.error(err);
    }
  });
};

module.exports.completeTaskController = (req, res) => {
  const updates = {
    status: "complete",
  };
  Task.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updatedTask) => res.send(updatedTask))
    .catch((err) => res.send(err));
};

module.exports.cancelTaskController = (req, res) => {
  const updates = {
    status: "cancelled",
  };
  Task.findByIdAndUpdate(req.params.id, updates, { new: true })
    .then((updatedTask) => res.send(updatedTask))
    .catch((err) => res.send(err));
};
