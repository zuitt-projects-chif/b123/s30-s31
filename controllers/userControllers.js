const User = require("../models/User");

module.exports.createUser = (req, res) => {
  console.log("body", req.body);

  User.findOne({ username: req.body.username }, (err, result) => {
    console.error("err", err);
    // console.log("result", result);
    if (result !== null && result.username === req.body.username) {
      return res.status(409).send("Duplicate username found!");
    } else {
      const doc = new User({
        username: req.body.username,
        password: req.body.password,
      });
      doc.save((err, result) => {
        if (err) {
          return console.error(err);
        } else {
          console.log("Saved", result);
          return res.send("Successful Registration!");
        }
      });
    }
  });
};
